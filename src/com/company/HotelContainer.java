package com.company;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

// For XML parsing based on response formatting

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "root")
public class HotelContainer {
    @XmlElement(name = "element")
    private List<Hotel> hotels;

    public List<Hotel> getHotels() {
        return hotels;
    }

    public void setHotels(List<Hotel> hotels) {
        this.hotels = hotels;
    }
}
