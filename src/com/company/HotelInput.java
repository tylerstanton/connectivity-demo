package com.company;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "root")
public class HotelInput {
    @XmlElement
    private String city;
    @XmlElement
    private String checkin;
    @XmlElement
    private String checkout;
    @XmlElement
    private String provider;

    // Needed for Jaxb
    public HotelInput() {
    }

    public HotelInput(String city, String checkin, String checkout, String provider) {
        this.city = city;
        this.checkin = checkin;
        this.checkout = checkout;
        this.provider = provider;
    }


    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCheckin() {
        return checkin;
    }

    public void setCheckin(String checkin) {
        this.checkin = checkin;
    }

    public String getCheckout() {
        return checkout;
    }

    public void setCheckout(String checkout) {
        this.checkout = checkout;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }
}
