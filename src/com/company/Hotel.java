package com.company;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name = "element")
@XmlAccessorType(XmlAccessType.FIELD)
public class Hotel {
    @XmlElement
    private int id;
    @XmlElement
    private String hotel_name;
    @XmlElement
    private long num_reviews;
    @XmlElement
    private int num_stars;
    @XmlElement
    private String address;

    @XmlElementWrapper
    @XmlElement(name = "element")
    private List<String> amenities;

    @XmlElement
    private String image_url;
    @XmlElement
    private double price;

    private double retail_price;

    public Hotel() {
    }

    public Hotel(int id, String hotel_name, long num_reviews, String address, int num_stars, List<String> amenities, String image_url, double price) {
        this.id = id;
        this.hotel_name = hotel_name;
        this.num_reviews = num_reviews;
        this.address = address;
        this.num_stars = num_stars;
        this.amenities = amenities;
        this.image_url = image_url;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public double getPrice() {
        return price;
    }

    public void setRetailPrice(double retail_price) {
        this.retail_price = retail_price;
    }

}
