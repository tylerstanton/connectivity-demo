package com.company;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;

public class HotelConnection {
    private HashMap<Integer, Hotel> idToHotelMap;

    public HotelConnection() {
        this.idToHotelMap = new HashMap<>();
    }

    // Post the request with its body, and return the response string (null if corrupt)
    protected static String postRequest(HttpURLConnection connection, String bodyStr) throws Exception {
        OutputStream os = connection.getOutputStream();
        byte[] body = bodyStr.getBytes(StandardCharsets.UTF_8);
        os.write(body, 0, body.length);
        os.flush();

        // Handle response (if ok)
        if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
            StringBuilder sb = new StringBuilder();
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8));
            String line;
            // Read in response to a string
            while ((line = br.readLine()) != null) {
                sb.append(line);
                sb.append(System.getProperty("line.separator"));
            }
            br.close();
            return sb.toString();
        }
        return null;
    }

    public void requestHotels(HotelInput hotelInput) {
        // For override
    }

    public HashMap<Integer, Hotel> getIdToHotelMap() {
        return idToHotelMap;
    }

    protected void addHotel(Hotel hotel) {
        idToHotelMap.put(hotel.getId(), hotel);
    }

    protected void addHotels(List<Hotel> hotels) {
        for (Hotel hotel : hotels) addHotel(hotel);
    }

    // Initialize httpurl connection with desired url and type
    protected HttpURLConnection createConnection(String url, String type) throws Exception {
        URL jsonURL = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) jsonURL.openConnection();
        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setRequestProperty("Content-Type", type);
        connection.setRequestProperty("Accept", type);
        connection.setRequestMethod("POST");
        return connection;
    }
}
