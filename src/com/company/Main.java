package com.company;

import com.google.gson.Gson;
import org.postgresql.util.PGobject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

// External Libraries Used - Gson, Postgresql, JAXB
// (gson-2.85, postgresql-42.2.8, javax.activation-api-1.2.0, jaxb-api-2.3.1, jaxb-osgi-2.4.0)

public class Main {
    private final static String PROVIDER = "snaptravel";
    private static Connection connection;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        try {
            // Read input
            System.out.println("City: ");
            String city = scanner.nextLine();
            System.out.println("Check In: ");
            String checkIn = scanner.nextLine();
            System.out.println("Check Out: ");
            String checkOut = scanner.nextLine();
            HotelInput hotelInput = new HotelInput(city, checkIn, checkOut, PROVIDER);

            // Generate hotels from json url
            HotelJsonConnection jsonHotelConnection = new HotelJsonConnection();
            jsonHotelConnection.requestHotels(hotelInput);

            // Generate hotels from xml url
            HotelXmlConnection hotelXmlConnection = new HotelXmlConnection();
            hotelXmlConnection.requestHotels(hotelInput);

            // Initialize local db connection
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "");
            // Store the duplicate hotels
            storeHotels(jsonHotelConnection.getIdToHotelMap(), hotelXmlConnection.getIdToHotelMap());
            connection.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    // Find duplicates, update json response with xml's price, call for storage
    private static void storeHotels(HashMap<Integer, Hotel> jsonHotelMap, HashMap<Integer, Hotel> xmlHotelMap) throws Exception {
        for (Map.Entry<Integer, Hotel> hotelEntry : xmlHotelMap.entrySet()) {
            Hotel hotel = jsonHotelMap.get(hotelEntry.getKey());
            if (hotel == null) continue; // Ignore non-duplicates
            hotel.setRetailPrice(hotelEntry.getValue().getPrice());
            storeHotel(hotel);
        }
    }

    // Store the hotel in the local database
    private static void storeHotel(Hotel hotel) throws Exception {
        //System.out.println(hotel.getId());
        Gson gson = new Gson();
        String json = gson.toJson(hotel);
        PGobject jsonObject = new PGobject();
        jsonObject.setType("json");
        jsonObject.setValue(json);
        PreparedStatement ps = connection.prepareStatement("insert into hotels (id, json) values(?, ?)");
        ps.setObject(1, hotel.getId());
        ps.setObject(2, jsonObject);
        ps.execute();
    }
}
