package com.company;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.HttpURLConnection;

public class HotelXmlConnection extends HotelConnection {
    private final static String XML_URL = "https://experimentation.getsnaptravel.com/interview/legacy_hotels";
    private final static String REQUEST_TYPE = "application/xml";

    @Override
    public void requestHotels(HotelInput hotelInput) {
        try {
            HttpURLConnection connection = createConnection(XML_URL, REQUEST_TYPE);
            String request = generateXMLRequest(hotelInput); // convert input object to xml
            String response = postRequest(connection, request);
            if (response == null) return;
            // convert xml response string to HotelContainer which holds a list of hotels
            // (Container class is needed based on xml response format)
            JAXBContext jaxbContext = JAXBContext.newInstance(HotelContainer.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            HotelContainer hotelContainer = (HotelContainer) jaxbUnmarshaller.unmarshal(new StringReader(response));
            addHotels(hotelContainer.getHotels());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    // Generates the XML body based on hotelInput
    private static String generateXMLRequest(HotelInput hotelInput) throws Exception {
        JAXBContext jaxbContext = JAXBContext.newInstance(HotelInput.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        StringWriter sw = new StringWriter();
        jaxbMarshaller.marshal(hotelInput, sw);
        return sw.toString();
    }

}
