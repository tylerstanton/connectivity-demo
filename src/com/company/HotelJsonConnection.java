package com.company;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import java.net.HttpURLConnection;

public class HotelJsonConnection extends HotelConnection {
    private final static String JSON_URL = "https://experimentation.getsnaptravel.com/interview/hotels";
    private final static String REQUEST_TYPE = "application/json";

    @Override
    public void requestHotels(HotelInput hotelInput) {
        try {
            Gson gson = new Gson();
            HttpURLConnection connection = createConnection(JSON_URL, REQUEST_TYPE);
            String request = gson.toJson(hotelInput); // convert input object to json
            String response = postRequest(connection, request);
            if (response == null) return;

            // convert response to Json Array
            JsonArray hotels = new JsonParser().parse(response).getAsJsonObject().getAsJsonArray("hotels");

            // add hotels to map
            for (int i = 0; i < hotels.size(); i++) {
                Hotel hotel = gson.fromJson(hotels.get(i), Hotel.class); // convert json obj to java obj
                addHotel(hotel);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
